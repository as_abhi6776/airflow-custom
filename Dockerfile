FROM apache/airflow:2.4.1

RUN export  AUTH_ROLE_PUBLIC = 'Admin'
RUN export AIRFLOW__WEBSERVER__RBAC=False